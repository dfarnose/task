<?php

namespace App\Http\Controllers;

use App\Http\Services\TaskService as ServicesTaskService;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->service = New ServicesTaskService();
    }

    public function store(Request $request){
        $this->service->store(
            $request->input('title'));
            return response('task created successfully', 200)->header('Content-Type', 'application/json');
    }

    public function update(Request $request){
        dd($request->input('id'));
        $updated = $this->service->update(
            $request->input('id'));
            if(!$updated){
                return response('error when updated Task, try later', 500)
                ->header('Content-Type', 'text/plain');
            }
            return response('Task updated suce', 200)
            ->header('Content-Type', 'text/plain');
        }

    public function get(Request $request){
        $list = $this->service->get();
        if(!$list) return [];
        return response()->json(['list' => $list]);
    }

    
    
}
