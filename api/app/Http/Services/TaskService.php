<?php
namespace App\Http\Services;
use App\Http\Repositories\TaskRepositorie;
use App\Models\Task;

 final class TaskService {
     
    public function __construct(){
        $this->repositorie = new TaskRepositorie();}
        
    public function store($title,$status = Task::STATUS_STOPED):void{
        $this->repositorie->create($title,$status);
    }
    public function update($id):bool{
       return $this->repositorie->update($id);
    }
    public function get(){
        return $this->repositorie->get();
    }
}
