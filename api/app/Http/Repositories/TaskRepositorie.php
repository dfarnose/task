<?php
namespace App\Http\Repositories;
use App\Models\Task;
use Illuminate\Support\Facades\DB;

final class TaskRepositorie {
    
    public function create($title,$status):void{
        Task::create([
            'title' => $title,
            'status' => Task::STATUS_STOPED
         ]);
    }
    public function update($id) {
        $task =  Task::where('id',$id)->first();
        if($task->status === Task::STATUS_ACTIVE){
            $task->status = Task::STATUS_STOPED;
            $task->save();
            return  true;
        } else {
            return $this->updateToActive($task->id);
        }
    }
    function updateToActive($id){
        $task =  Task::find($id);
        Task::where('status',Task::STATUS_ACTIVE)
        ->update(['status' => Task::STATUS_STOPED]);
        $task->status = Task::STATUS_ACTIVE;
        $task->save();
        return true;
    }
    public function get(){
        return  Task::orderBY('status','desc')
        ->orderBY('updated_at','desc')
        ->orderBY('created_at','desc')
        ->get();
    }

}
