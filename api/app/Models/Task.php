<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    CONST STATUS_ACTIVE = 1;
    CONST STATUS_STOPED = 0;


    protected $table = 'tasks';
    public $timestamps = true;
    protected $fillable = [
        'title',
        'status'
    ];
}
