<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\Task;

class TaskManagementTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

     /**  @test */
    use DatabaseMigrations,RefreshDatabase;

    public function a_task_can_be_created() {

        $this->withoutExceptionHandling();
        
        $response = $this->post('/task',[
            'title' => 'new task',
            'status' => 0
        ]);
        $response->assertOK();
    
        $this->assertCount(1, Task::all());
        $task = Task::first();
        $this->assertEquals($task->title,'new task');
        $this->assertEquals($task->status,0);
    }

     /**  @test */

     public function a_task_can_be_updated_to_active_and_all_other_shoudl_becomo_no_active(){
        
        $this->withoutExceptionHandling();

         
        Task::factory()->create([
            'id' => 1,
            'title' => 'Tets',
            'status' => 1
        ]);
        Task::factory()->create([
            'id' => 2,
            'title' => 'Tets2',
            'status' => 0
        ]);
        Task::factory()->create([
            'id' => 3,
            'title' => 'Tets3',
            'status' => 0
        ]);

        $response = $this->post('/taskUpdate',[
            'id' => 3
        ]);

        $task3 = Task::find(3);
        $task2 = Task::find(2);
        $task1 = Task::find(1);

        $this->assertEquals(1,$task3->status);
        $this->assertEquals(0,$task2->status);
        $this->assertEquals(0,$task1->status);
        
    }
    /**  @test */
    
    public function a_list_of_task_can_be_retrieved(){
        $this->withoutExceptionHandling();
        Task::factory()->count(3)->create();
        $response = $this->get('/getTasks');
        $response->assertOK();
        $this->assertCount(3, Task::all());
    }
}
