import React,{useRef} from 'react'
import axios from 'axios'
import "./Task.css";

const Task = (list)=> {
    const idTask = useRef(list.list.id);
    const handleStartTime = (e) =>{
        update(idTask.current.id);
    }
    const handleStopTime = (e) =>{
        console.log(e.target.id);
    }
    const update = async (id) => {
       
        let url = 'http://localhost:8000/taskUpdate'
        await axios.post(url,id)
        .then((response)=> {
            console.log(response);

        }).catch((error)=> {
              console.log(error);
          });
        }
     return (
    <div className="task">
        <h5>Task: {list.list.title} </h5>
        <h4>Time: {list.list.time}</h4>
        <div className="task__body">
            <div id={list.list.id} ref={idTask} onClick={handleStartTime} className="start"><i  className="far fa-play-circle"></i></div>
            <div id={list.list.id} ref={idTask} onClick={handleStopTime} className="stop"><i  className="far fa-pause-circle"></i></div>
        </div>
     </div>
    )
}
export default Task;

