import {ToastContainer,toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'
import React from "react";

export const notifySuccess = (msg) => {
    if (!toast.isActive('success')) {
        toast.info(msg,{
            toastId:'success',
            position: "top-right",
            autoClose: 5000,
            className : 'toast-size',
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: false
        });
    }
};

export const notifyFailed = (msg) => {
    if (!toast.isActive('failed')) {
        toast.error(msg, {
            toastId: 'failed',
            position: "top-right",
            autoClose: 5000,
            className: 'toast-size',
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: false
        });
    }
}
    export const Toast = () => {
        return (
            <ToastContainer hideProgressBar={false}/>
        )
    };
