import React,{useState} from 'react'
import axios from 'axios'
import {ToastContainer,toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'
import "./Form.css";

export default function Form() {
    const [value,setValue] = useState();
    const handleChange =(e) => {
        setValue({...value,[e.target.name]:e.target.value})
    }
    const create = async () => {
              let url = 'http://localhost:8000/task'
              await axios.post(url,value).then((response)=> {
                  toast(response.data);
                }).catch((error)=> {
                    console.log(error);
                });
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        create();
    }
    return (<form onSubmit={handleSubmit}>
            <div className="form-control">
            <label htmlFor="title">Title</label>
            <input type='text' 
                name="title" 
                required   
                className="form-control"  
                onChange={handleChange}/>
                </div>
                    <div className="form-group">
                    <button type="submit" className="btn primary">Save Task</button>
                </div>
            <ToastContainer/>
            </form>
    )
}
