import React,{useEffect,useState} from 'react'
import "./List.css";
import Task from "./Task";


const  List = (lists) => {
     return (
      <div className="list__task">
        {lists.list.map(list => <Task list={list} key={list.title}/>)}
      </div>
    )
  }
  
  export default List;
