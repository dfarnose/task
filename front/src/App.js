import './App.css';
import React,{useEffect,useState} from 'react';
import ReactDOM from 'react-dom';
import Form from "./components/Form";
import List from "./components/List";
import axios from 'axios'

function App() {
  const [tasks,setTaks] = useState([]);
  useEffect(() => {
      async function fetchTasks() {
      let url = 'http://localhost:8000/getTasks'
      const response = await axios.get(url);
      setTaks(response?.data?.list || []);
     
  }
  fetchTasks();
}); // Or [] if effect doesn't need props or state

  return (
    <div className="container">
      <Form setTaks={setTaks}/>
      <List list={tasks}/>
    </div>
  );
}

export default App;
